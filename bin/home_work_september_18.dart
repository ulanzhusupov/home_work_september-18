// ignore_for_file: constant_pattern_never_matches_value_type

void main(List<String> arguments) {
  // print(isItWhole(15, 2));
  // print(countAmountCharacter("Hello"));
  // print(whatTypeIsIt(52));
  // print(whatTypeIsIt(5.2));
  // print(whatTypeIsIt("52"));
  // print(isNumberSimple(5));
  print(whatMonth(5));
}

String isItWhole(int a, int b) {
  return (a % b != 0) ? "Не делится нацело" : "Делится нацело";
}

int countAmountCharacter(String str) {
  return str.length;
}

String whatTypeIsIt(dynamic variable) {
  switch (variable.runtimeType.toString()) {
    case "int":
      return "Целое число";
    case "double":
      return "Дробное число";
    case "String":
      return "Строка";
    default:
      return "Не распознан тип переменной";
  }
}

bool isNumberSimple(int number) {
  if (number <= 1) {
    return false; // Числа меньше или равные 1 не считаются простыми.
  }
  if (number <= 3) {
    return true; // 2 и 3 считаются простыми числами.
  }
  if (number % 2 == 0 || number % 3 == 0) {
    return false; // Числа, кратные 2 или 3, не простые.
  }

  // Проверяем делители, начиная с 5 и увеличивая с шагом 6.
  // Это оптимизация для уменьшения количества проверок.
  for (int i = 5; i * i <= number; i += 6) {
    if (number % i == 0 || number % (i + 2) == 0) {
      return false; // Если число делится на i или i + 2, то оно не простое.
    }
  }

  return true; // Если не найдено делителей, то число простое.
}

String whatMonth(int month) {
  if (month == 12 || (month >= 1 && month <= 2)) {
    return "Зима";
  } else if (month >= 3 && month <= 5) {
    return "Весна";
  } else if (month >= 6 && month <= 8) {
    return "Лето";
  } else if (month >= 9 && month <= 11) {
    return "Осень";
  }

  return "Убедитесь, что ввели число от 1 до 12";
}
